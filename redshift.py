import pandas as pd
import boto3
import s3fs
            
class db_write:
    def post_process(self,result):
        columns=[]
        for i in range(len(result['ColumnMetadata'])):
            columns.append(result['ColumnMetadata'][i]['name'])
        rows = []
        for r in result['Records']:
            tmp = []
            for c in r:
                tmp.append(c[list(c.keys())[0]])
            rows.append(tmp)
        return pd.DataFrame(rows, columns=columns)


    def execute_dql(self,query) :
        dbclient = boto3.client('redshift-data',region_name='ap-south-1')
        query_response = dbclient.execute_statement(
        Database = 'dev',
        DbUser='pwdev',
        ClusterIdentifier='redshift-cluster-dev',
        Sql = query
        )
        query_id = query_response["Id"]
        desc = None
        while True:
            desc = dbclient.describe_statement(Id=query_id)
            if desc["Status"] == "FINISHED":
                break
            if desc["Status"] == "FAILED":
                raise Exception(desc["Error"])
        df = None
        if desc and desc["ResultRows"]  > 0:
            result = dbclient.get_statement_result(Id=query_id)
            df = self.post_process(result)
        return df