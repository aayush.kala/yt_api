#!/usr/bin/env python
# coding: utf-8
import os
import json
import re
import uvicorn
import string
import s3fs
import boto3

import numpy as np
import pandas as pd

from fastapi import FastAPI
from nltk.corpus import words
from pydantic import BaseModel
from datetime import datetime, timedelta
from googleapiclient.discovery import build

from my_utils import Utils
from nlp import nlp
from hugging_face import sentimentanalysis
from redshift import db_write



nlp = nlp()
sen = sentimentanalysis()
redshift_write = db_write()








class VideoInput(BaseModel):
    
    video_id: str

        
def fetch_youtube_data(video_id):

    #### Youtube Credentials ####
    api_key = "AIzaSyCN4cSCz-ocJbxUBLMNMyM0dBdln6wzDk4"
    youtube = build('youtube', 'v3', developerKey=api_key)
    
    #### Define the video ID ####
    video_id = video_id
    
    comments_data = []
    
    #### Video information ####
    video_response = youtube.videos().list(
        part="snippet",
        id=video_id
    ).execute()
    
    #### Channel information ####
    channel_id = video_response['items'][0]['snippet']['channelId']
    
    channel_response = youtube.channels().list(
        part="snippet",
        id=channel_id
    ).execute()
    
    channel_name = channel_response['items'][0]['snippet']['title']
    
    #### Comments for the video ####
    comments_response = youtube.commentThreads().list(
        part="snippet",
        videoId=video_id,
        maxResults=1
    ).execute()
    
    while comments_response:
        for comment_item in comments_response['items']:
            comment = comment_item['snippet']['topLevelComment']['snippet']
            comment_id = comment_item['id']
            reply_count = comment_item['snippet']['totalReplyCount']
            is_reply = 1 if 'parentId' in comment_item['snippet'] else 0
            comment_data = {
                'Channel ID': channel_id,
                'Channel Name': channel_name,
                'Video ID': video_id,
                'Comment ID': comment_id,
                'Timestamp': comment['publishedAt'],
                'User ID': comment['authorChannelId']['value'],
                'User Name': comment['authorDisplayName'],
                'Chat/Comment': comment['textDisplay'],
                'Like Count': comment['likeCount'],
                'Reply Count': reply_count,
                'is_Reply': is_reply
                
            }
            comments_data.append(comment_data)
    
            #### Comment replies for the comment ####
            replies_response = youtube.comments().list(
                part="snippet",
                parentId=comment_id,
                maxResults=100
            ).execute()
    
            for reply_item in replies_response['items']:
                reply = reply_item['snippet']
                reply_data = {
                    'Channel ID': channel_id,
                    'Channel Name': channel_name,
                    'Video ID': video_id,
                    'Comment ID': comment_id,
                    'Timestamp': reply['publishedAt'],
                    'User ID': reply['authorChannelId']['value'],
                    'User Name': reply['authorDisplayName'],
                    'Chat/Comment': reply['textDisplay'],
                    'Like Count': reply['likeCount'],
                    'Reply Count': 0,
                    'is_Reply': 1
                }
                comments_data.append(reply_data)
    
        if 'nextPageToken' in comments_response:
            page_token = comments_response['nextPageToken']
            comments_response = youtube.commentThreads().list(
                part="snippet",
                videoId=video_id,
                maxResults=100,
                pageToken=page_token
            ).execute()
        else:
            break
    
    df = pd.DataFrame(comments_data)
    #### Creating Primary Key ####
    df["_id"] = df["Comment ID"]+df["Timestamp"]
    
    

    df["_id"] = df["Comment ID"]+df["Timestamp"]
    df["_id"] = df["_id"].astype(str) 
    columns = ['_id'] + df.columns[:-1].tolist()
    df = df[columns]
    
    return df

    
    pass

def perform_sentiment_analysis(data):
    
    
   
    df_1 = data.copy(deep=True)
    df_1.rename(columns={'Chat/Comment': 'text'}, inplace=True)
    df_2 = df_1.copy(deep=True)
    df_2['text'] = df_2['text'].astype(str)

    #### Data Cleaning ####
    df_2['comments'] = df_2['text'].apply(lambda x: Utils.html_clean(x))
    df_2['comments'] = df_2['comments'].apply(lambda x: Utils.clean_equations(x))
    df_2['comments'] = df_2['comments'].apply(lambda x: Utils.remove_stopwords(x))
    df_2['comments'] = df_2['comments'].apply(lambda x: Utils.special_edge_cases(x))
    df_2['comments'] = df_2['comments'].apply(lambda x: Utils.combine(x))
    df_2['en_text'] = df_2['comments'].apply(lambda x :Utils.translation(x))
   
    #### NLP Part ####
    df_2['en_text'] = df_2['en_text'].astype(str)
    df_2['is_interrogative'] = df_2['en_text'].apply(lambda x : nlp.is_question(x))
    df_2_neg = df_2[df_2['is_interrogative']==True]
    
    df_2_neg = df_2_neg.drop('is_interrogative', axis=1)
    df_2_neg['label'] = 'NEG'
    df_2_neg['score'] = 0.91
    df = df_2[df_2['is_interrogative']==False]

    #### Sentimet Analysis ####
    data = sen.analysis(df)
    final = pd.concat([df_2_neg,data])

    final.rename(columns={'text':'original_text','Timestamp':'video_timestamp'},inplace=True)
    
    return final 
    pass

def write_to_database(data):

    data.to_csv("YT.csv",index=False)
    #### Write to S3 ####
    data.to_csv(f's3://data-jupyter-app/aayush_kala@pw.live/YT_data/YT.csv',index=None)

    ####Write to Redshift ####
    redshift_write.execute_dql(f"/*bq_redshift*/ copy public.youtube_test_new from 's3://data-jupyter-app/aayush_kala@pw.live/YT_data/YT.csv' IAM_ROLE 'arn:aws:iam::539436019547:role/jupiterhub-role' REGION 'ap-south-1'  IGNOREHEADER 1 dateformat 'auto' TIMEFORMAT 'auto' CSV QUOTE as '\"' delimiter ',' TRUNCATECOLUMNS;")


    pass


app = FastAPI()

@app.get('/')
def index():
    return {'message': 'Welcome to YT Sentiment Analysis'}


@app.post("/analyze")

def analyze_video(input_data: VideoInput):
    video_id = input_data.video_id
 
    #### Fetch YouTube data ####
    data = fetch_youtube_data(video_id)
    
    #### Perform sentiment analysis ####
    sentiments = perform_sentiment_analysis(data)
    
    
    #### Write data to the database ####
    write_to_database(sentiments)
    
    return {"message": "Data fetched, sentiment analysis performed, and data written to the database."}
if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)
