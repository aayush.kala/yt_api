import nltk
import pickle
filename = 'nltk_model.sav'
nltk_model = pickle.load(open(filename, 'rb'))
question_types = ["whQuestion","ynQuestion"]

class nlp:
	
	def dialogue_act_features(self,post):
		features = {}
		for word in nltk.word_tokenize(post):
			features['contains({})'.format(word.lower())] = True
		return features
	
	def is_ques_using_nltk(self,ques):
		question_type = nltk_model.classify(self.dialogue_act_features(ques)) 
		return question_type in question_types
	
	
	def is_question(self,question):
		

		#nltk.download('nps_chat')
		#posts = nltk.corpus.nps_chat.xml_posts()[:10000]
	    
		question_pattern = ["do i", "do you", "what", "who", "is it", "why","would you", "how","is there",
	                    "are there", "is it so", "is this true" ,"to know", "is that true", "are we", "am i", 
	                   "question is", "tell me more", "can i", "can we", "tell me", "can you explain",
	                   "question","answer", "questions", "answers", "ask","will be","will there be",'which']

		helping_verbs = ["is","am","can", "are", "do", "does"]
		question = question.lower().strip()
		if not self.is_ques_using_nltk(question):
		    is_ques = False
		    # check if any of pattern exist in sentence
		    for pattern in question_pattern:
		        is_ques  = pattern in question
		        if is_ques:
		            break	
		    # there could be multiple sentences so divide the sentence
		    sentence_arr = question.split(".")
		    for sentence in sentence_arr:
		        if len(sentence.strip()):
		            # if question ends with ? or start with any helping verb
		            # word_tokenize will strip by default
		            first_word = nltk.word_tokenize(sentence)[0]
		            if sentence.endswith("?") or first_word in helping_verbs:
		                is_ques = True
		                break
		    return is_ques    
		else:
		    return True		
