# utils.py
import re
import emoji
from bs4 import BeautifulSoup
import spacy
from nltk.tokenize import word_tokenize
from englisttohindi.englisttohindi import EngtoHindi
from googletrans import Translator


class Utils:
    @staticmethod
    def html_clean(comment):
        text = comment.lower()
        text = re.sub(r'@[A-Za-z0-9]+','',text)
        text = BeautifulSoup(text,'lxml').get_text()
        #text = re.sub(r'\b\w{2}\b', '', text)
        text = re.sub(r'&#39;','\'',text)
        text = re.sub('\d','number', text)
        text = re.sub(r'https(//)[A-Za-z0-9.]*(/)[A-Za-z0-9]+','',text)
        text = emoji.demojize(text, delimiters=("", " "))
        text = re.sub(r'_','',text)
        
        return text
    @staticmethod
    def remove_stopwords(comment):
        

        ## Stop Words List
        
        stop = set([])
        cls = spacy.util.get_lang_class('en')
        cls.Defaults.stop_words = stop
        text = word_tokenize(comment)
        text = [word for word in text if not word in stop]
        return text
        ##To clear data of equation
    @staticmethod
    def clean_equations(string):
        symbols = ['>','<','-','+','-','^','*','/','>=','=<','<=','%',':','|']
        special_sym = ['?',"'",'_','@','#','$','`','~',',','.',';','(',')','{','}','[',']','"']
        
        for substr in string.split():
               
            if len(substr)>1:
    
    
                for i in list(substr):
    
                    if i in symbols :
    
                        string = (string.replace(substr,'equation'))
                        
                    if i.isalpha() == False and i not in special_sym :
                        
                        string = (string.replace(substr,'equation'))
        
        return string
    ## Some special characters did appear after clean eauqtion function application
    @staticmethod
    def special_edge_cases(comment):
        text = [re.sub('[^a-zA-Z0-9]+', '', x) for x in comment]
        
        text = [re.sub('[?()#$]+/', '', x) for x in comment]
        
        return text
    @staticmethod   
    def translation(text):
        res = EngtoHindi(text)
        translator = Translator()
        eng_word = Translator().translate(res.convert,dest='english').text
            #print(eng_word)
        return eng_word

    
    @staticmethod    
    def combine(text):
        return ' '.join(text)

