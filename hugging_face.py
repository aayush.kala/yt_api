import pickle
import pandas as pd
filename = 'tokenizer.sav'
tokenizer = pickle.load(open(filename, 'rb'))
            # print("Tokenizer loaded")

filename = 'huggingface.sav'
huggingface_model = pickle.load(open(filename, 'rb'))

class sentimentanalysis:
    def analysis(self, df):
            
        try:
            

            token_lens = []
            batch_size = 10000
            token_lens = [len(tokens) for batch in (list(df['en_text'])[i:i+batch_size] for i in range(0, len(df['en_text']), batch_size)) for txt in batch for tokens in [tokenizer.encode(txt, max_length=512, truncation=True)]]

            result = [huggingface_model(batch) for batch in (list(df['en_text'])[i:i+batch_size] for i in range(0, len(df['en_text']), batch_size))]

            sentiment_result = pd.DataFrame([item for sublist in result for item in sublist])


            df = df.reset_index(drop=True)
            #print("done3")
            df['label'] = pd.Series([x for x in sentiment_result['label']])
            #print("done4")
            df['score'] = pd.Series([x for x in sentiment_result['score']])
            #print("done5")

            df = df.drop(['is_interrogative'], axis=1)
            #print("done6")
            print("Sentiment analysis done")
            return df

        except Exception as e:
            print("An error occurred:", str(e))
            raise  # Optional: Re-raise the exception for further handling
